from tkinter.ttk import Entry
from tkinter import IntVar

class IntEntry(Entry):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = IntVar()
        self.configure(textvariable=self.value)

    def get(self):
        return self.value.get()

    def set(self, newValue):
        self.value.set(newValue)