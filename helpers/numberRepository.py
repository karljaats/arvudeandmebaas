from pickle import dump, load

class NumberRepository:
    """
    Aitab meeles hoida arve ja leiab nende eri suurusi.
    Võimaldab arve salvestada/lugeda failist
    """
    def __init__(self):
        """

        :return: uus repositoorium
        """
        self.numbers = []

    def add(self, newNumber):
        """
        Uue arvu lisamine repositooriumisse
        :param newNumber: lisatav arv
        :return: None
        """
        self.numbers.append(newNumber)

    def min(self):
        return min(self.numbers)

    def max(self):
        return max(self.numbers)

    def avg(self):
        return sum(self.numbers) / len(self.numbers)

    def saveToFile(self, filename):
        f = open(filename, 'w')
        dump(self.numbers, f)
        f.close()

    def loadFromFile(self, filename):
        f = open(filename)
        self.numbers = load(f)
        f.close()