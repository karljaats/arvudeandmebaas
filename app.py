from tkinter import *
from tkinter import ttk
from helpers import IntEntry, NumberRepository

class App(Tk):
    def __init__(self):
        super().__init__()
        self.title("Arvude Andmebaas")
        self.addElements()
        self.numbers = NumberRepository()

    def addElements(self):
        self.newNumberLabel = ttk.Label(text='Uus arv:')
        self.newNumberLabel.grid(sticky=W)

        self.newNumber = IntEntry()
        self.newNumber.grid(row=1)

        self.addNumberBtn = ttk.Button(text='Lisa', command=self.addNewNumber)
        self.addNumberBtn.grid(row=1, column=1)

    def addNewNumber(self):
        self.numbers.add(self.newNumber.get())
        print(self.numbers.avg())